using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Cookie
{
    public class FallingCookiePool : MonoBehaviour
    {
        [SerializeField] private FallingCookie FallingCookiePrefab;
        [SerializeField] private float _leftSpawnBorder;
        [SerializeField] private float _rightSpawnBorder;
        [SerializeField] private float _bottomSpawnBorder;
        [SerializeField] private float _topSpawnBorder;
        private LinkedList<FallingCookie> _fallingCookies = new();
        private LinkedList<FallingCookie> _nonFallingCookies = new();

        public void SpawnCookie()
        {
            RefreshFallingCookiesList();
            FallingCookie cookie;
            if (_nonFallingCookies.Count == 0)
            {
                cookie = CreateCookie();
            }
            else
            {
                cookie = _nonFallingCookies.Last.Value;
                _nonFallingCookies.RemoveLast();
            }
            var cookiePosition = GetSpawnPosition();
            cookie.transform.SetLocalPositionAndRotation(cookiePosition, Quaternion.identity);
            cookie.StartFalling();
            _fallingCookies.AddLast(cookie);
        }

        private FallingCookie CreateCookie()
        {
            var cookie = Instantiate(FallingCookiePrefab, this.transform);
            return cookie;
        }

        private Vector3 GetSpawnPosition()
        {
            float x = Random.Range(_leftSpawnBorder, _rightSpawnBorder);
            float y = Random.Range(_bottomSpawnBorder, _topSpawnBorder);
            return new Vector3(x, y, 0);
        }

        private void RefreshFallingCookiesList()
        {
            if (_fallingCookies.Count == 0) return;
            var fallingCookie = _fallingCookies.First;
            while (fallingCookie.Next != null)
            {
                if (!fallingCookie.Value.IsFalling)
                {
                    var cookieToRemove = fallingCookie;
                    fallingCookie = fallingCookie.Next;
                    _fallingCookies.Remove(cookieToRemove);
                    _nonFallingCookies.AddLast(cookieToRemove);
                }
                else
                {
                    fallingCookie = fallingCookie.Next;
                }
            }
        }

        private void Awake()
        {
            if (_leftSpawnBorder > _rightSpawnBorder)
                (_leftSpawnBorder, _rightSpawnBorder) = (_rightSpawnBorder, _leftSpawnBorder);
            if (_bottomSpawnBorder > _topSpawnBorder)
                (_bottomSpawnBorder, _topSpawnBorder) = (_topSpawnBorder, _bottomSpawnBorder);
        }

        private void OnEnable()
        {
            MainCookie.MainCookieClicked += SpawnCookie;
        }

        private void OnDisable()
        {
            MainCookie.MainCookieClicked -= SpawnCookie;
        }
    }
}