using System;
using UnityEngine;

namespace Assets.Scripts.Game.Cookie
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class FallingCookie : MonoBehaviour
    {

        public bool IsFalling { get; private set; }
        private SpriteRenderer _renderer;
        private const float CHECK_FALLING_COOLDOWN = 0.5f;
        private float timeToNextCheckSeconds = 0;
        public void StartFalling()
        {
            gameObject.SetActive(true);
            IsFalling = true;
            ResetTimer();
        }

        private void CheckStillFalling()
        {
            if (!IsFalling) return;
            if (_renderer.isVisible == false)
            {
                IsFalling = false;
                gameObject.SetActive(false);
            }
            ResetTimer();
        }

        private void ResetTimer()
        {
            timeToNextCheckSeconds = CHECK_FALLING_COOLDOWN;
        }

        private void Awake()
        {
            TryGetComponent(out _renderer);
        }

        private void Update()
        {
            timeToNextCheckSeconds = Math.Max(0, timeToNextCheckSeconds - Time.deltaTime);
            if (IsFalling && timeToNextCheckSeconds == 0)
            {
                CheckStillFalling();
            }
        }
    }
}