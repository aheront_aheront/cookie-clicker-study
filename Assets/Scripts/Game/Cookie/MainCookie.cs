using System;
using UnityEngine;
using Assets.Scripts.Game.Granny;
using Unity.VisualScripting;

namespace Assets.Scripts.Game.Cookie
{
    public class MainCookie : MonoBehaviour
    {
        public static event Action MainCookieClicked;
        public static event Action MainCookieClickedWhenGrannyWasBothering;

        private static MainCookie _instance;

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
        }

        private void OnMouseDown()
        {
            if (GameState.Instance.State != GameStates.Going) return;

            if (BotheringGranny.IsBotheringToCook == false)
                MainCookieClicked?.Invoke();
            else
                MainCookieClickedWhenGrannyWasBothering?.Invoke();
        }
    }
}