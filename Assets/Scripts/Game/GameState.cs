﻿using Assets.Scripts.Game.Statistics;
using System;

namespace Assets.Scripts.Game
{
    public enum GameStates
    {
        Going,
        Won
    }
    public class GameState
    {
        public event Action GameStateChanged;
        public GameStates State
        {
            get => _state;
            private set
            {
                _state = value;
                GameStateChanged?.Invoke();
            }
        }
        public static GameState Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new GameState();
                return _instance;
            }
        }
        private const int SCORE_TO_WIN = 500;
        private static GameState _instance;
        private GameStates _state = GameStates.Going;


        public GameState()
        {
            ScoreKeeper.Instance.ScoreChanged += OnScoreChanged;
        }

        private void OnScoreChanged()
        {
            if (ScoreKeeper.Instance.Score > SCORE_TO_WIN)
            {
                State = GameStates.Won;
            }
        }

        ~GameState()
        {
            ScoreKeeper.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}
