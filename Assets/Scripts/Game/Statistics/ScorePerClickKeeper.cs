﻿using System;

namespace Assets.Scripts.Game.Statistics
{
    public class ScorePerClickKeeper
    {
        public event Action ScorePerClickChanged;

        public static ScorePerClickKeeper Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ScorePerClickKeeper();
                return _instance;
            }
        }

        public int ScorePerClick
        {
            get
            {
                return _scorePerClick;
            }
            private set
            {
                if (_scorePerClick != value)
                {
                    _scorePerClick = value;
                    ScorePerClickChanged?.Invoke();
                }
            }
        }

        private static ScorePerClickKeeper _instance;
        private int _scorePerClick = 0;
        private const int SCORE_UPGRADE_PER_SCORE = 20;

        public ScorePerClickKeeper()
        {
            ScoreKeeper.Instance.ScoreChanged += OnScoreChanged;
            OnScoreChanged();
        }

        private void OnScoreChanged()
        {
            int score = ScoreKeeper.Instance.Score;
            ScorePerClick = 1 + score / SCORE_UPGRADE_PER_SCORE;
        }

        ~ScorePerClickKeeper()
        {
            ScoreKeeper.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}
