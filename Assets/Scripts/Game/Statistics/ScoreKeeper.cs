using Assets.Scripts.Game.Cookie;
using System;

namespace Assets.Scripts.Game.Statistics
{
    public class ScoreKeeper
    {
        public event Action ScoreChanged;

        public int Score
        {
            get
            {
                return _score;
            }
            private set
            {
                if (_score != value)
                {
                    _score = value;
                    ScoreChanged?.Invoke();
                }
            }
        }
        public static ScoreKeeper Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ScoreKeeper();
                return _instance;
            }
        }

        private static ScoreKeeper _instance;
        private const int FINE_WHEN_CLICK_WHILE_GRANNY_BOTHERING = 10;
        private const float FINE_WHEN_CLICK_WHILE_GRANNY_BOTHERING_COEF_PER_LEVEL = 1.5f;
        private int _score = 0;

        public ScoreKeeper()
        {
            MainCookie.MainCookieClicked += OnMainCookieClicked;
            MainCookie.MainCookieClickedWhenGrannyWasBothering += OnMainCookieClickedWhenGrannyWasBothering;
        }

        private void OnMainCookieClicked()
        {
            Score += ScorePerClickKeeper.Instance.ScorePerClick;
        }

        private void OnMainCookieClickedWhenGrannyWasBothering()
        {
            int fine;
            if (CurrentLevelKeeper.Instance.CurrentLevel == 1)
            {
                fine = 0;
            }
            else
            {
                fine = (int)(FINE_WHEN_CLICK_WHILE_GRANNY_BOTHERING *
                    Math.Pow(FINE_WHEN_CLICK_WHILE_GRANNY_BOTHERING_COEF_PER_LEVEL,
                             CurrentLevelKeeper.Instance.CurrentLevel - 1));
            }
            Score -= fine;
        }

        ~ScoreKeeper()
        {
            MainCookie.MainCookieClicked -= OnMainCookieClicked;
            MainCookie.MainCookieClickedWhenGrannyWasBothering -= OnMainCookieClickedWhenGrannyWasBothering;
        }
    }
}