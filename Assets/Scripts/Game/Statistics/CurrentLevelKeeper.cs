using System;
using UnityEngine;

namespace Assets.Scripts.Game.Statistics
{
    public class CurrentLevelKeeper 
    {
        public event Action CurrentLevelChanged;
        public int CurrentLevel {
            get 
            {
                return _currentLevel;
            }   
            private set
            {
                if(_currentLevel != value)
                {
                    _currentLevel = value;
                    CurrentLevelChanged?.Invoke();
                }
            }
        }

        public static CurrentLevelKeeper Instance
        {
            get
            {
                if(_instance == null)
                    _instance = new CurrentLevelKeeper();
                return _instance;
            }    
        }

        private static CurrentLevelKeeper _instance;
        private int _currentLevel = 0;

        public CurrentLevelKeeper()
        {
            ScoreKeeper.Instance.ScoreChanged += OnScoreChanged;
            OnScoreChanged();
        }

        private void OnScoreChanged()
        {
            int score = ScoreKeeper.Instance.Score;
            if (score > 400)
                CurrentLevel = 3;
            else if (score > 100)
                CurrentLevel = 2;
            else
                CurrentLevel = 1;
        }

        ~CurrentLevelKeeper()
        {
            ScoreKeeper.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}