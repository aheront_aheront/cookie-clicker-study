using Assets.Scripts.Game.Cookie;
using System;
using UnityEngine;

namespace Assets.Scripts.Game.Statistics
{
    public class ClicksCountKeeper
    {
        public event Action ClicksCountChanged;
        public int ClicksCount
        {
            get
            {
                return _clicksCount;
            }
            private set
            {
                if (_clicksCount != value)
                {
                    _clicksCount = value;
                    ClicksCountChanged?.Invoke();
                }
            }
        }
        public static ClicksCountKeeper Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ClicksCountKeeper();
                return _instance;
            }
        }

        private static ClicksCountKeeper _instance;
        private int _clicksCount = 0;

        public ClicksCountKeeper()
        {
            MainCookie.MainCookieClicked += OnMainCookieClicked;
        }

        private void OnMainCookieClicked()
        {
            ClicksCount = ClicksCount + 1;
        }

        ~ClicksCountKeeper()
        {
            MainCookie.MainCookieClicked -= OnMainCookieClicked;
        }
    }
}