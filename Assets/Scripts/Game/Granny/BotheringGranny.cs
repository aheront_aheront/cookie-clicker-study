using Assets.Scripts.Game.Statistics;
using UnityEngine;

namespace Assets.Scripts.Game.Granny
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class BotheringGranny : MonoBehaviour
    {

        public static bool IsBotheringToCook;
        private static BotheringGranny _instance;
        private const float BOTHERING_DURANCE = 5f;
        private const float NON_BOTHERING_DURANCE = 5f;
        private const int SCORE_TO_GRANNY_ACTIVATION = 20;

        private SpriteRenderer _renderer;
        private float _timeToBotheringChange = 0f;

        private void Awake()
        {
            if (_instance == null)
                _instance = this;
            else
                Destroy(gameObject);
            TryGetComponent(out _renderer);
        }

        private void Update()
        {
            UpdateBotheringState();
            UpdateSpriteRenderer();
        }
        
        private void UpdateBotheringState()
        {
            if (GameState.Instance.State != GameStates.Going) return;
            if (ScoreKeeper.Instance.Score < SCORE_TO_GRANNY_ACTIVATION)
            {
                IsBotheringToCook = false;
                return;
            }
            _timeToBotheringChange -= Time.deltaTime;
            if(_timeToBotheringChange <= 0)
            {
                if (IsBotheringToCook)
                {
                    _timeToBotheringChange = NON_BOTHERING_DURANCE;
                }
                else
                {
                    _timeToBotheringChange = BOTHERING_DURANCE;
                }
                IsBotheringToCook = !IsBotheringToCook;
            }

        }

        private void UpdateSpriteRenderer()
        {
            if(IsBotheringToCook && _renderer.enabled == false)
            {
                _renderer.enabled = true;
            }
            if(!IsBotheringToCook && _renderer.enabled == true)
            {
                _renderer.enabled = false;
            }
        }
    }
}