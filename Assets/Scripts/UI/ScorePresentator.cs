using Assets.Scripts.Game.Statistics;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ScorePresentator : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            ScoreKeeper.Instance.ScoreChanged += OnScoreChanged;
            OnScoreChanged();
        }

        private void OnScoreChanged()
        {
            _text.text = $"����: {ScoreKeeper.Instance.Score}";
        }

        private void OnDisable()
        {
            ScoreKeeper.Instance.ScoreChanged -= OnScoreChanged;
        }
    }
}