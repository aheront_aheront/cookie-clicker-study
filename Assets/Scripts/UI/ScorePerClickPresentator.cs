using Assets.Scripts.Game.Statistics;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ScorePerClickPresentator : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            ScorePerClickKeeper.Instance.ScorePerClickChanged += OnScorePerClickChanged;
            OnScorePerClickChanged();
        }

        private void OnScorePerClickChanged()
        {
            _text.text = $"+{ScorePerClickKeeper.Instance.ScorePerClick}";
        }

        private void OnDisable()
        {
            ScorePerClickKeeper.Instance.ScorePerClickChanged -= OnScorePerClickChanged;
        }
    }
}