using Assets.Scripts.Game.Statistics;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ClicksCountPresentator : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            ClicksCountKeeper.Instance.ClicksCountChanged += OnClickCountChanged; ;
            OnClickCountChanged();
        }

        private void OnClickCountChanged()
        {
            _text.text = $"Количество кликов: {ClicksCountKeeper.Instance.ClicksCount}";
        }

        private void OnDisable()
        {
            ClicksCountKeeper.Instance.ClicksCountChanged -= OnClickCountChanged;
        }
    }
}
