using Assets.Scripts.Game;
using Assets.Scripts.Game.Statistics;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class WinText : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
            _text.enabled = false;
        }

        private void OnEnable()
        {
            GameState.Instance.GameStateChanged += OnGameStateChanged;
            OnGameStateChanged();
        }

        private void OnGameStateChanged()
        {
            if (GameState.Instance.State == GameStates.Won)
            {
                _text.enabled = true;
                _text.text = "���� ���������, �� ��������!";
            }
        }

        private void OnDisable()
        {
            ScoreKeeper.Instance.ScoreChanged -= OnGameStateChanged;
        }
    }
}