﻿using Assets.Scripts.Game.Statistics;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Scripts.UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class CurrentLevelPresentator : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        private void Awake()
        {
            TryGetComponent(out _text);
        }

        private void OnEnable()
        {
            CurrentLevelKeeper.Instance.CurrentLevelChanged += OnCurrentLevelChanged;
            OnCurrentLevelChanged();
        }

        private void OnCurrentLevelChanged()
        {
            _text.text = $"Уровень: {CurrentLevelKeeper.Instance.CurrentLevel}";
        }

        private void OnDisable()
        {
            CurrentLevelKeeper.Instance.CurrentLevelChanged -= OnCurrentLevelChanged;
        }
    }
}
